void main() {
  //
  var business_app = new The_app();

  business_app.name_of_the_app = "Khula";
  business_app.category = "Agriculture Solution";
  business_app.developer = "Karidas Tshintsnolo and Matthew Piper";
  business_app.year = 2018;

  business_app.app_information();
}

// create a class
class The_app {
  String? name_of_the_app;
  String? category;
  String? developer;
  int? year;

// create the function inside the class
  void app_information() {
    //convert the name of the app to upper case

    print("The name of the app: ${name_of_the_app?.toUpperCase()}");
    print("The category: ${category}");
    print("The developer name: ${developer}");
    print("The year it won MTN Business App of the Year Awards: ${year}");
  }
}
